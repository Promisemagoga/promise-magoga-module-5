import 'package:flutter/material.dart';
import 'package:flutter_application_1/Dashboard.dart';
import 'package:flutter_application_1/FutureScreen1.dart';
import 'package:flutter_application_1/FutureScreen2.dart';
import 'package:flutter_application_1/UserProfileEdit.dart';
import 'package:flutter_application_1/addAppointment.dart';



class Dashboard extends StatelessWidget {
  const Dashboard ({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
       theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.pink
        ),
      ),
      home: Scaffold(
        appBar: AppBar(
          
          title:const Text('Dashboard'),
        ),
        body: Column(children: [
           
              SizedBox(
                height: 130,
                child: FloatingActionButton(
                  onPressed:() {
                    Navigator.push(context,
          MaterialPageRoute(builder: (context) => FutureScreen1()));
                  },
                  child: Text('1'),
                ),
              ),
               SizedBox(
                height: 130,
                child: FloatingActionButton(
                  onPressed:() {
                    Navigator.push(context,
          MaterialPageRoute(builder: (context) => FutureScreen2()));
                  },
                   child: Text('2'),
                )
               ),
                SizedBox(
                height: 130,
                child: FloatingActionButton(
                  onPressed:() {
                    Navigator.push(context,
          MaterialPageRoute(builder: (context) => UserProfileEdit()));
                  },
                   child: Text('Profile'),
                )
                ),
                 SizedBox(
                height: 130,
                child: FloatingActionButton(
                  onPressed:() {
                    Navigator.push(context,
          MaterialPageRoute(builder: (context) => addAppointment()));
                  },
                   child: Text('add'),
                )
               ),
        ]
        )
        )
      );
  }

}



    