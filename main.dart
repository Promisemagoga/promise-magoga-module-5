// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';

import 'Login.dart';

 Future main() async {
WidgetsFlutterBinding.ensureInitialized();
await Firebase.initializeApp(
  options: FirebaseOptions(
  apiKey: "AIzaSyAX1Aa2NToMQfT-f0vgtGrKmVihOtjX8jQ",
  authDomain: "promise-magoga-module-5-2ca40.firebaseapp.com",
  projectId: "promise-magoga-module-5-2ca40",
  storageBucket: "promise-magoga-module-5-2ca40.appspot.com",
  messagingSenderId: "398855854067",
  appId: "1:398855854067:web:6f4cdebe405de69bb32486")

);
   runApp(MyApp());
    }


class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Department of health library',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.pink
        ),
              ),
      home: AnimatedSplashScreen(
        splash: 'images/doctor-examining-patient-clinic-illustrated_23-2148856559.jpg',
        nextScreen: Login(),
        splashTransition: SplashTransition.rotationTransition,
        duration: 3000,
        backgroundColor: Colors.pink,
      )
    );
  }
}


