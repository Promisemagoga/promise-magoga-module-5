import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/AppointmentList.dart';
import 'package:flutter_application_1/Dashboard.dart';

class addAppointment extends StatefulWidget {
  const addAppointment({Key? key}) : super(key: key);

  @override
  State<addAppointment> createState() => _addAppointmentState();
}

class _addAppointmentState extends State<addAppointment> {
  @override
  Widget build(BuildContext context) {
    TextEditingController AppointmentController = TextEditingController();
    TextEditingController DateController = TextEditingController();

    Future _addAppointments() {
      final Appointment = AppointmentController.text;
      final Date = DateController.text;

      final ref = FirebaseFirestore.instance.collection("Appointments").doc();

      return ref
          .set({
            "Doctors Appointments": Appointment,
            "Date of appointment": Date,
            "doc_id": ref.id
          })
          .then((value) => log("Collection added!"))
          .catchError((onError) => log(onError));
    }

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.pink),
        ),
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Add Appointments'),
            ),
            body: SingleChildScrollView(
              child: Center(
                  child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 150.0),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                        child: TextField(
                          controller: AppointmentController,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              hintText:
                                  "The appointment the doctor have with a patient"),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                        child: TextField(
                          controller: DateController,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              hintText: "Enter Date"),
                        ),
                      ),
                      ElevatedButton(
                          onPressed: () {
                            _addAppointments();
                          },
                          child: Text("Add Appointment"))
                    ],
                  ),
                 AppointmentList()
                ],
              )),
            )));
  }
}
