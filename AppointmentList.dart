import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class AppointmentList extends StatefulWidget {
  const AppointmentList({Key? key}) : super(key: key);

  @override
  State<AppointmentList> createState() => _AppointmentListState();
}

class _AppointmentListState extends State<AppointmentList> {
  final Stream<QuerySnapshot> _Appointments =
      FirebaseFirestore.instance.collection("Appointments").snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _Appointments,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                height: (MediaQuery.of(context).size.height),
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  children: snapshot.data!.docs
                      .map((DocumentSnapshot documentSnapshot) async {
                        Map<String, dynamic> data =
                            documentSnapshot.data() as Map<String, dynamic>;
                        return Column(children: [
                          Card(
                              child: Column(children: [
                            ListTile(
                              title: Text(data['Appointment']),
                              subtitle: Text(data['Date']),
                            ),
                            ButtonTheme(
                              child: ButtonBar( 
                                children: [
                                  OutlineButton.icon(
                                    onPressed:  () {},
                                    icon: Icon(Icons.edit),
                                    label: Text("Edit"),
                                    )
                                ],
                              )
                            
                            )
                          ]))
                        ]);
                      })
                      .toList().cast(),
                      
                ),
              ))
            ],
          );
        } else
          return (Text("No data"));
      },
    );
  }
}
